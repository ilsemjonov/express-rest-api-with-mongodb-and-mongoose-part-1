const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require("mongoose");
const favRouter = express.Router();
favRouter.use(bodyParser.json());
var authenticate = require('../authenticate');
const cors = require("./cors");
const Favorite = require('../models/favorite');

favRouter.route('/')
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
.get(cors.cors , authenticate.verifyUser, (req,res,next) => {
    Favorite.findOne({user : req.user._id})
    .populate('user')
    .populate('dishes')
    .then( favorites =>{

            if(!favorites || favorites.dishes.length == 0){
                res.statusCode = 404;
                res.end('No favourites item present');
            }
            else
            {

                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(favorites);
            }
    },err =>{next(err)})
    .catch((err) => next(err));
})
.post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Favorite.findOne({user : req.user._id})
    .then(favorites =>{

        if(!favorites)
        {
            Favorite.create({user : req.user._id , dishes : req.body},(err,favorite) =>{
                if(!err)
                {
                    console.log('Favorites Created and dishes added ', favorite);
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(favorite);
                }
                else
                return next(err);
            });
        }
        else
        {
            var favdishes = [];
            favorites.dishes.forEach(dishID =>{
                favdishes.push(dishID.toString());
            });

            console.log(favdishes);
            req.body.forEach( (dishID) =>{
                
                if(favdishes.indexOf(dishID._id) <0)
                {
                    favorites.dishes.push(dishID._id);
                }
            })

            favorites.save((err,favorite) =>{
                if(!err)
                {
                    console.log('Favorites dishes added', favorite);
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(favorite);
                }
                else
                return next(err);
            })
        }
    },err =>{next(err)})
    .catch((err) => next(err));
})
.put(cors.corsWithOptions, authenticate.verifyUser,(req, res, next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported on /favorites');
})
.delete(cors.corsWithOptions, authenticate.verifyUser,(req, res, next) => {
    Favorite.remove({user : req.user._id})
    .then((resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
    .catch((err) => next(err));    
});

favRouter.route('/:dishID')
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
.get(cors.cors , authenticate.verifyUser, (req,res,next) => {
    Favorite.findOne({user : req.user._id})
    .populate('user')
    .populate('dishes')
    .then( favorites =>{

            if(!favorites || favorites.dishes.length == 0){
                res.statusCode = 404;
                res.end('No favourites item present');
            }
            else
            {
                for(var i=0;i<favorites.dishes.length;i++)
                {
                    if(favorites.dishes[i]._id == req.params.dishID)
                    {
                        res.statusCode = 200;
                        res.setHeader('Content-Type', 'application/json');
                        res.json(favorites.dishes[i]);
                        return;
                    }
                }
                var error = new Error("Dish is not found in the favorites dishes!");
                error.statusCode = 404;
                return next(error);
            }
    },err =>{next(err)})
    .catch((err) => next(err));
})
.post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Favorite.findOne({user : req.user._id})
    .then(favorites =>{
        if(!favorites)
        {
            Favorite.create({user : req.user._id,dishes : [req.params.dishID]}, (err,favorite) =>{
                if(!err)
                {
                    console.log('Favorites Created and dish added ', favorite);
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(favorite);
                }
                else
                return next(err);
            })
        }
        else
        {
            if(favorites.dishes.indexOf(req.params.dishID) <0)
            {
                favorites.dishes.push(req.params.dishID);
            }
            favorites.save((err,favorite) =>{
                if(!err)
                {
                    console.log('Favorites dish added', favorite);
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(favorite);
                }
                else
                return next(err);
            })
        }
    },err =>{next(err)})
    .catch((err) => next(err));
})
.put(cors.corsWithOptions, authenticate.verifyUser,(req, res, next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported');
})
.delete(cors.corsWithOptions, authenticate.verifyUser,(req, res, next) => {
    Favorite.findOne({user : req.user._id})
    .then((favorites) => {

        if(favorites)
        {
            const index = favorites.dishes.indexOf(req.params.dishID);
            if (index > -1) {
                favorites.dishes.splice(index, 1);
                favorites.save((err,favorite) =>{
                    if(!err)
                    {
                        res.statusCode = 200;
                        res.setHeader('Content-Type', 'application/json');
                        res.json(favorites);
                    }
                    else
                    return next(err);
                })
            }
            else
            {
                var err = new Error("Dish is not present in favorites");
                err.statusCode = 404;
                return naxt(err);
            }
        }
        else
        {
            var err = new Error("Favorites not found");
            err.statusCode = 404;
            return next(err);
        }
    }, (err) => next(err))
    .catch((err) => next(err));    
});

module.exports = favRouter;
