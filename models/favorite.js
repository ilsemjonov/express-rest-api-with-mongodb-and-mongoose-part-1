var mongoose = require('mongoose');
require('mongoose-currency').loadType(mongoose);
var Schema = mongoose.Schema;

var favSchema = new Schema({
    user : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'User'
    },
    dishes :[
        {
            type : mongoose.Schema.Types.ObjectId,
            ref : 'Dish'
        }
    ]
}, {
    timestamps: true
});

var Favorite = mongoose.model('Favorite', favSchema);

module.exports = Favorite;